# textstyle

The `textstyle`utility can be used to detect a variety of flaws int UTF-8 encoded text files.

## Installation

```bash
git clone https://alexanderveit@bitbucket.org/alexanderveit/tools.git
cd tools
make
```

The compiled binary can then be copied to its final destination, e.g.

```bash
cp obj/textstyle /usr/local/bin/
```

## Usage

```
Usage: textstyle [OPTION]... [FILE]...

Check source code for its conformance to common rules.

  With no FILE, or when FILE is -, read standard input.

  This program requires to be run with an UTF-8 locale

  -b, --line-break=[LF|CRLF|MIXED]  set the line break style, default is LF
  -l, --max-line-length=[+]NUM      set the maximum line length, by default
                                    line length checks are disabled
      --mll-grace-limit=[+]NUM      the maximum number of characters the line
                                    length may be exceeded before an error is
                                    reported, default is 0
  -m, --allow-bom                   allow byte order marks
  -t, --tab=[+]NUM                  allow tab characters and set the tab
                                    width for calculating line lengths
  -u, --tab-leading-only            use tabs only for leading indentations
                                    (this option requires --tab, -t to be
                                    present)
  -1, --exit-first-error            exit after the first error
      --ignore-trailing-ws          ignore trailing whitespace
      --stderr                      send messages to stderr instead of stdout
      --help                        display this help and exit
      --version                     print version information and exit

  If rule violations have been found the exit status is 1.
```