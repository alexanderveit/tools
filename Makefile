

GIT_VERSION := $(shell git describe --always --tags --dirty='(dirty)')
BUILD_TIME  := $(shell date --utc '+%Y-%m-%d %H:%M UTC')


SRCDIR   := src
BUILDDIR := obj


CC      := gcc
CFLAGS  := -std=c99 -march=native -O3 -Wall -DBUILD_INFO="$(GIT_VERSION) $(BUILD_TIME)"
LDFLAGS :=


all: directories $(BUILDDIR)/textstyle

directories:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/textstyle: $(BUILDDIR)/textstyle.o
	$(CC) -o $(BUILDDIR)/textstyle $(BUILDDIR)/textstyle.o

$(BUILDDIR)/textstyle.o: $(SRCDIR)/textstyle.c
	$(CC) $(CFLAGS) -c -o $(BUILDDIR)/textstyle.o  $(SRCDIR)/textstyle.c

clean:
	rm -rf $(BUILDDIR)

.PHONY: clean

