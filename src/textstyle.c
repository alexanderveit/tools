/*
   Copyright (C) 2017 Alexander Veit.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#define _XOPEN_SOURCE 700


#include <limits.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <wchar.h>
#include <wctype.h>
#include <unistd.h>
#include <locale.h>
#include <assert.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <linux/fadvise.h>


//-----------------------------------------------------------------------------

#define VERSION "0.1.0"

#ifdef BUILD_INFO
  #define STR(x) #x
  #define STRX(x) STR(x)
  #define VERSIONEX VERSION " "  STRX(BUILD_INFO)
#else
  #define VERSIONEX VERSION
#endif


#define UNSET_MAX_LINE_LENGTH           -1
#define UNSET_MLL_GRACE_LIMIT           -1
#define UNSET_CHECKED_MAX_LINE_LENGTH   -1
#define DEFAULT_BOM_ALLOWED             false
#define UNSET_TAB_WIDTH                 1
#define DEFAULT_TAB_ALLOWED             false
#define DEFAULT_TABS_LEADING_ONLY       false
#define DEFAULT_IGNORE_TRAILING_WS      false
#define DEFAULT_EXIT_FIRST_ERROR        false

static const size_t BUF_SIZE = 8192;

//-----------------------------------------------------------------------------
// types
//-----------------------------------------------------------------------------

typedef struct tParserState ParserState;

typedef enum tBOM {NONE, UTF_8, UTF_16LE, UTF_16BE, UTF_32LE, UTF_32BE} BOM;

typedef enum tLBStyle {UNKNOWN, LF, CRLF, MIXED} LBStyle;

//-----------------------------------------------------------------------------
// function prototypes
//-----------------------------------------------------------------------------

static void
setProgName(const char *p_argv0);

static void
showUsage(int p_status);

static void
showVersion(void);

static void
getOptions(int p_argc, char *p_argv[]);

static void
init(ParserState *p_pps, const char *p_fileName);

static void
newLine(ParserState *p_pps);

static BOM
getBom(char p_buf[], size_t p_read);

static void
checkMixedLineBreaks(ParserState *p_pps);

static void
checkTrailingWhitespace(ParserState *p_pps);

static void
checkLineLength(ParserState *p_pps);

static void
error(const ParserState *p_pps, const char *p_fmt, ...);

static bool
isWhiteSpace(wchar_t p_wc);

static bool
isControlCharacter(wchar_t p_wc);

static int
process(const char* p_fileName, FILE *p_fp);

static void
atExitHandler(void);

//-----------------------------------------------------------------------------
// program name
//-----------------------------------------------------------------------------

static const char *g_progName = "";

void
setProgName(const char *p_argv0)
{
  const char *slash;

  if (p_argv0 == NULL)
  {
    fputs("A NULL argv[0] was passed through an exec system call.\n", stderr);
    abort();
  }

  slash      = strrchr(p_argv0, '/');
  g_progName = slash != NULL ? slash + 1 : p_argv0;
}

//-----------------------------------------------------------------------------
// configuration parameters
//-----------------------------------------------------------------------------

static FILE *g_errout;

static bool g_bBomAllowed = DEFAULT_BOM_ALLOWED;

static LBStyle g_lineBreakStyle = LF;

static bool g_bTabAllowed = DEFAULT_TAB_ALLOWED;

static bool g_bTabsLeadingOnly = DEFAULT_TABS_LEADING_ONLY;

static bool g_bIgnoreTrailingWS = DEFAULT_IGNORE_TRAILING_WS;

static bool g_bExitFirstError = DEFAULT_EXIT_FIRST_ERROR;

static uintmax_t g_tabWidth = UNSET_TAB_WIDTH;

static uintmax_t g_maxLineLength = UNSET_MAX_LINE_LENGTH;

static uintmax_t g_mllGraceLimit = UNSET_MLL_GRACE_LIMIT;

static uintmax_t g_checkedMaxLineLength = UNSET_CHECKED_MAX_LINE_LENGTH;

//-----------------------------------------------------------------------------
// user interface
//-----------------------------------------------------------------------------

void
showUsage(int p_status)
{
  if (p_status != EXIT_SUCCESS)
  {
    fprintf(stderr, "Try '%s --help' for more information.\n", g_progName);
  }
  else
  {
    fprintf(stdout, "\
Usage: %s [OPTION]... [FILE]...\n\
\n\
Check source code for its conformance to common rules.\n\
\n\
  With no FILE, or when FILE is -, read standard input.\n\
\n\
  This program requires to be run with an UTF-8 locale\n\
\n\
  -b, --line-break=[LF|CRLF|MIXED]  set the line break style, default is LF\n\
  -l, --max-line-length=[+]NUM      set the maximum line length, by default\n\
                                    line length checks are disabled\n\
      --mll-grace-limit=[+]NUM      the maximum number of characters the line\n\
                                    length may be exceeded before an error is\n\
                                    reported, default is 0\n\
  -m, --allow-bom                   allow byte order marks\n\
  -t, --tab=[+]NUM                  allow tab characters and set the tab\n\
                                    width for calculating line lengths\n\
  -u, --tab-leading-only            use tabs only for leading indentations\n\
                                    (this option requires --tab, -t to be\n\
                                    present)\n\
  -1, --exit-first-error            exit after the first error\n\
      --ignore-trailing-ws          ignore trailing whitespace\n\
      --stderr                      send messages to stderr instead of stdout\n\
      --help                        display this help and exit\n\
      --version                     print version information and exit\n\
\n\
  If rule violations have been found the exit status is 1.\n\
", g_progName);
  }

  exit(p_status);
}


void showVersion(void)
{
  fprintf(stdout, "\
%s %s\n\
Copyright (C) 2017 Alexander Veit\n\
License GPLv3+: GNU GPL version 3 or later\
 <http://gnu.org/licenses/gpl.html>.\n\
This is free software: you are free\
 to change and redistribute it.\n\
There is NO WARRANTY, to the extent\
 permitted by law.\n\
", g_progName, VERSIONEX);

  exit(EXIT_SUCCESS);
}


enum tLongOptChar
{
  OPT_MLL_GRACE_LIMIT    = (CHAR_MIN - 2),
  OPT_IGNORE_TRAILING_WS = (CHAR_MIN - 3),
  OPT_STDRR              = (CHAR_MIN - 4),
  OPT_HELP_CHAR          = (CHAR_MIN - 5),
  OPT_VERSION_CHAR       = (CHAR_MIN - 6)
};

static struct option const OPTIONS[] =
{
  {"line-break", required_argument, NULL, 'b'},
  {"max-line-length", required_argument, NULL, 'l'},
  {"mll-grace-limit", required_argument, NULL, OPT_MLL_GRACE_LIMIT},
  {"allow-bom", no_argument, NULL, 'm'},
  {"tab", required_argument, NULL, 't'},
  {"tab-leading-only", no_argument, NULL, 'u'},
  {"ignore-trailing-ws", no_argument, NULL, OPT_IGNORE_TRAILING_WS},
  {"exit-first-error", no_argument, NULL, '1'},
  {"stderr", no_argument, NULL, OPT_STDRR},
  {"help", no_argument, NULL, OPT_HELP_CHAR},
  {"version", no_argument, NULL, OPT_VERSION_CHAR},
  {NULL, 0, NULL, 0}
};


void
getOptions(int p_argc, char *p_argv[])
{
  int l_opt;

  while ((l_opt = getopt_long(p_argc,
                              p_argv,
                              "b:l:mt:u1",
                              OPTIONS,
                              NULL)) != -1)
  {
    switch (l_opt)
    {
      case 'b':
        {
          if (optarg == NULL)
            showUsage(EXIT_FAILURE);

          if (strcmp("LF", optarg) == 0 || strcmp("lf", optarg) == 0)
            g_lineBreakStyle = LF;
          else if (strcmp("CRLF", optarg) == 0 || strcmp("crlf", optarg) == 0)
            g_lineBreakStyle = CRLF;
          else if (strcmp("MIXED", optarg) == 0 || strcmp("mixed", optarg) == 0)
            g_lineBreakStyle = MIXED;
          else
            showUsage(EXIT_FAILURE);
        }
        break;

      case 'l':
        {
          if (optarg == NULL)
            showUsage(EXIT_FAILURE);

          const intmax_t l_maxLineLength = atoll(optarg);

          if (l_maxLineLength < 0)
            showUsage(EXIT_FAILURE);

          g_maxLineLength = l_maxLineLength;
        }
        break;

      case OPT_MLL_GRACE_LIMIT:
        {
          if (optarg == NULL)
            showUsage(EXIT_FAILURE);

          const intmax_t l_mllGraceLimit = atoll(optarg);

          if (l_mllGraceLimit < 0)
            showUsage(EXIT_FAILURE);

          g_mllGraceLimit = l_mllGraceLimit;
        }
        break;

      case 'm':
        g_bBomAllowed = true;
        break;

      case 't':
        {
          if (optarg == NULL)
            showUsage(EXIT_FAILURE);

          const intmax_t l_tabWidth = atoll(optarg);

          if (l_tabWidth < 0)
            showUsage(EXIT_FAILURE);

          g_tabWidth    = l_tabWidth;
          g_bTabAllowed = true;
        }
        break;

      case 'u':
        g_bTabsLeadingOnly = true;
        break;

      case '1':
        g_bExitFirstError = true;
        break;

      case OPT_IGNORE_TRAILING_WS:
        g_bIgnoreTrailingWS = true;
        break;

      case OPT_STDRR:
        g_errout = stderr;
        break;

      case OPT_VERSION_CHAR:
        showVersion();
        break;

      case OPT_HELP_CHAR:
        showUsage(EXIT_SUCCESS);
        break;

      default:
        showUsage(EXIT_FAILURE);
        break;
    }
  }

  if (g_maxLineLength != UNSET_MAX_LINE_LENGTH)
  {
    if (g_mllGraceLimit == UNSET_MLL_GRACE_LIMIT)
      g_checkedMaxLineLength = g_maxLineLength;
    else
      g_checkedMaxLineLength = g_maxLineLength + g_mllGraceLimit;
  }
  else
  {
    g_checkedMaxLineLength = UNSET_CHECKED_MAX_LINE_LENGTH;
  }
}


//-----------------------------------------------------------------------------


struct tParserState
{
  const char *fileName;
  int         status;
  uintmax_t   line;
  uintmax_t   col;
  uintmax_t   lineLength;
  LBStyle     lbsFound;
  wchar_t     wcLast;
  uintmax_t   lws; // linear whitespace
  uintmax_t   llb; // linear line break
  bool        bNonTabSeen;
  bool        bMixedLineBreaks;
};


void
init(ParserState *p_pps, const char *p_fileName)
{
  p_pps->fileName = p_fileName;
  p_pps->status = EXIT_SUCCESS;
  p_pps->line = 1;
  p_pps->col = 0;
  p_pps->lineLength = 0;
  p_pps->lbsFound = UNKNOWN;
  p_pps->wcLast = L'\0';
  p_pps->lws = 0;
  p_pps->llb = 0;
  p_pps->bNonTabSeen = false;
  p_pps->bMixedLineBreaks = false;
}


void
newLine(ParserState *p_pps)
{
  p_pps->line++;
  p_pps->col = 0;
  p_pps->lineLength = 0;
  p_pps->lws = 0;
  p_pps->llb++;
  p_pps->bNonTabSeen = false;
}


BOM
getBom(char p_buf[], size_t p_read)
{
  if (p_read < 2)
    return NONE; // not enough input data

  if (p_read >= 3 &&
      p_buf[0] == (char) 0xEF &&
      p_buf[1] == (char) 0xBB &&
      p_buf[2] == (char) 0xBF)
  {
    // UTF-8:  EF BB BF
    return UTF_8;
  }
  else if (p_read >= 2 &&
           p_buf[0] == (char) 0xFE &&
           p_buf[1] == (char) 0xFF)
  {
    // UTF-16 (BE):  FE FF
    return UTF_16BE;
  }
  else if (p_read >= 2 &&
           p_buf[0] == (char) 0xFF &&
           p_buf[1] == (char) 0xFE)
  {
    if (p_read >= 4 &&
        p_buf[2] == (char) 0x00 &&
        p_buf[3] == (char) 0x00)
    {
      // UTF-32 (LE):  FF FE 00 00
      return UTF_32LE;
    }
    else
    {
      // UTF-16 (LE):  FF FE
      return UTF_16LE;
    }
  }
  else if (p_read >= 4 &&
           p_buf[0] == (char) 0x00 &&
           p_buf[1] == (char) 0x00 &&
           p_buf[2] == (char) 0xFE &&
           p_buf[3] == (char) 0xFF)
  {
    // UTF-32 (BE):  00 00 FE FF
    return UTF_32BE;
  }
  else
  {
    return NONE;
  }
}


void
checkMixedLineBreaks(ParserState *p_pps)
{
  if (g_lineBreakStyle == MIXED)
    return; // do not check if mixed line breaks are allowed

  if (p_pps->bMixedLineBreaks)
    return; // report only once

  switch (p_pps->lbsFound)
  {
    case UNKNOWN:
      p_pps->lbsFound = p_pps->wcLast == L'\r' ? CRLF : LF;
      break;

    case LF:
      if (p_pps->wcLast == L'\r')
      {
        error(p_pps, "Mixed line break.");

        p_pps->lbsFound = MIXED;
        p_pps->status   = EXIT_FAILURE;
      }
      break;

    case CRLF:
      if (p_pps->wcLast != L'\r')
      {
        error(p_pps, "Mixed line break.");

        p_pps->lbsFound = MIXED;
        p_pps->status   = EXIT_FAILURE;
      }
      break;

    case MIXED:
      break; // cannot occur
  }
}


void
checkTrailingWhitespace(ParserState *p_pps)
{
  if (g_bIgnoreTrailingWS)
    return;

  if (p_pps->lws > 0)
  {
    error(p_pps, "Trailing whitespace.");

    p_pps->status = EXIT_FAILURE;
  }
}


void
checkLineLength(ParserState *p_pps)
{
  if (g_checkedMaxLineLength != UNSET_CHECKED_MAX_LINE_LENGTH &&
      p_pps->lineLength > g_checkedMaxLineLength)
  {
    error(p_pps,
          "%jd exceeded the maximum line length %jd.",
          p_pps->lineLength,
          g_maxLineLength); // do not include the grace limit

    p_pps->status = EXIT_FAILURE;
  }
}


bool
isWhiteSpace(wchar_t p_wc)
{
  return p_wc == L' ' || p_wc == L'\t';
}


bool
isControlCharacter(wchar_t p_wc)
{
  if (p_wc == L' ' || p_wc == L'\t' || p_wc == L'\n' || p_wc == L'\r')
    return false;

  return iswcntrl(p_wc);
}


int
process(const char* p_fileName, FILE *p_fp)
{
  ParserState l_ps;
  char        l_buf[BUF_SIZE];
  mbstate_t   l_state;
  bool        l_bFirstRead;
  size_t      l_pos;
  size_t      l_read;

  init(&l_ps, p_fileName);

  memset(&l_state, '\0', sizeof(l_state));

  l_bFirstRead = true;

  while ((l_read = fread(l_buf, sizeof(char), sizeof(l_buf), p_fp)) > 0)
  {
    l_pos = 0;

    // check for a byte order mark at the beginning of input
    if (l_bFirstRead)
    {
      BOM l_bom = getBom(l_buf, l_read);

      if (l_bom == NONE)
      {
        // nothing to do
      }
      else if (l_bom == UTF_8)
      {
        // UTF-8:  EF BB BF
        l_pos = 3;

        if (!g_bBomAllowed)
        {
          error(&l_ps, "UTF-8 byte order mark detected.");

          l_ps.status = EXIT_FAILURE;
        }
      }
      else if (l_bom == UTF_16BE)
      {
        // UTF-16 (BE):  FE FF
        error(&l_ps, "Unsupported UTF-16 (BE) byte order mark detected.");

        return EXIT_FAILURE;
      }
      else if (l_bom == UTF_32LE)
      {
        // UTF-32 (LE):  FF FE 00 00
        error(&l_ps, "Unsupported UTF-32 (LE) byte order mark detected.");

        return EXIT_FAILURE;
      }
      else if (l_bom == UTF_16LE)
      {
        // UTF-16 (LE):  FF FE
        error(&l_ps, "Unsupported UTF-16 (LE) byte order mark detected.");

        return EXIT_FAILURE;
      }
      else if (l_bom == UTF_32BE)
      {
        // UTF-32 (BE):  00 00 FE FF
        error(&l_ps, "Unsupported UTF-32 (BE) byte order mark detected.");

        return EXIT_FAILURE;
      }
      else
      {
        error(&l_ps, "Unexpected byte order mark detected.");

        return EXIT_FAILURE; // cannot happen
      }

      l_bFirstRead = false;
    }

    while (l_pos < l_read)
    {
      wchar_t l_wc;
      size_t  l_conv;

      l_conv = mbrtowc(&l_wc, &l_buf[l_pos], l_read - l_pos, &l_state);

      // handle special conversion cases
      if (l_conv == 0) // converted null character
      {
        l_ps.col++;

        error(&l_ps, "Null character found.");

        l_ps.status = EXIT_FAILURE;

        l_pos += 1; // see C99, 5.2.1.2

        continue; // skip it
      }
      else if (l_conv == (size_t) -2) // incomplete multibyte character
      {
        l_pos = l_read;

        continue; // next read operation
      }
      else if (l_conv == (size_t) -1) // invalid multibyte sequence
      {
        // state is undefined - must return
        error(&l_ps, "Invalid multibyte sequence found.");

        return EXIT_FAILURE; // state is undefined, cannot recover
      }

      l_pos += l_conv;

      if (l_wc == L'\n')
      {
        checkMixedLineBreaks(&l_ps);
        checkLineLength(&l_ps);
        checkTrailingWhitespace(&l_ps);

        // advance to the next line
        newLine(&l_ps);

        continue;
      }

      // next character in line
      l_ps.col++;

      if (l_ps.wcLast == L'\r')
      {
        error(&l_ps, "CR-only line break found.");

        l_ps.status = EXIT_FAILURE;

        continue;
      }

      if (l_wc == L'\r' && g_lineBreakStyle == LF)
      {
        error(&l_ps, "CR character found.");

        l_ps.status = EXIT_FAILURE;

        continue; // skip it
      }

      if (isControlCharacter(l_wc))
      {
        error(&l_ps, "Control character 0x%02X found.", (l_wc & 0xFF));

        l_ps.status = EXIT_FAILURE;

        continue; // skip it
      }

      // at this point we have neither a LF nor a CR nor a control character
      assert(l_wc != L'\n' && l_wc != L'\r');

      if (isWhiteSpace(l_wc))
        l_ps.lws++;
      else
        l_ps.lws = 0;

      if (l_wc == L'\t')
      {
        l_ps.lineLength += g_tabWidth;

        if (!g_bTabAllowed)
        {
          error(&l_ps, "TAB character found.");

          l_ps.status = EXIT_FAILURE;
        }
        else if (l_ps.bNonTabSeen && g_bTabsLeadingOnly)
        {
          error(&l_ps, "TAB character after non-TAB character found.");

          l_ps.status = EXIT_FAILURE;
        }
      }
      else
      {
        l_ps.lineLength++;
        l_ps.bNonTabSeen = true;
      }

      l_ps.wcLast = l_wc;
    }
  }

  checkTrailingWhitespace(&l_ps);

  return l_ps.status;
}


int
main(int p_argc, char *p_argv[])
{
  int l_status;

  g_errout = stdout;

  atexit(atExitHandler);
  setProgName(p_argv[0]);
  setlocale(LC_ALL, "");

  getOptions(p_argc, p_argv);

  l_status = EXIT_SUCCESS;

  if (optind == p_argc)
  {
    l_status = process("stdin", stdin);
  }
  else
  {
    for (int i = optind; i < p_argc; i++)
    {
      const char *l_fn;
      FILE       *l_fp;

      l_fn = p_argv[i];
      l_fp = NULL;

      if (strcmp("-", l_fn) == 0)
      {
        l_fp = stdin;
      }
      else
      {
        struct stat l_stat;

        if (stat(l_fn, &l_stat) == 0)
        {
          if (S_ISREG(l_stat.st_mode) || S_ISLNK(l_stat.st_mode))
          {
            l_fp = fopen(l_fn, "r");

            posix_fadvise(fileno(l_fp), 0, 0, POSIX_FADV_SEQUENTIAL);
          }
          else
          {
            fprintf(stderr, "%s is not a file.\n", l_fn);
          }
        }
        else
        {
          perror(l_fn);
        }
      }

      if (l_fp != NULL)
      {
        const int l_ret = process(l_fn, l_fp);

        fclose(l_fp);

        if (l_ret != EXIT_SUCCESS)
          l_status = l_ret;
      }
    }
  }

  return l_status;
}


void
error(const ParserState *p_pps, const char *p_fmt, ...)
{
  va_list l_args;

  if (p_pps->fileName != NULL)
    fprintf(g_errout, "%s: ", p_pps->fileName);

  fprintf(g_errout, "line %jd, col %jd: ", p_pps->line, p_pps->col);

  va_start(l_args, p_fmt);
  vfprintf(g_errout, p_fmt, l_args);
  va_end(l_args);

  fputc('\n', g_errout);

  if (g_bExitFirstError)
    exit(EXIT_FAILURE);
}


void
atExitHandler(void)
{
  if (fclose(stdout) != 0)
  {
    fprintf(stderr, "Error while closing stdout.\n");
    fclose(stderr);

    _exit(EXIT_FAILURE);
  }

  if (fclose(stderr) != 0)
    _exit(EXIT_FAILURE);
}
